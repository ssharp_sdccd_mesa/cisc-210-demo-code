"use strict";

import { Animal } from './Animal';

export class Cat extends Animal {
    private theColor: string;

    public constructor(name: string, color: string) {
        super(name); // call the super class constructor and pass in the name parameter
        this.theColor = color;
    }

    public color(): void {
        console.log(`${this.name} the cat's color is ${this.theColor}.`);
    }
}
