"use strict";

import { Color } from './Color';
import { NoisyAnimal } from './NoisyAnimal';

export class Animal implements Color, NoisyAnimal {
    protected name: string;

    public constructor(name: string) {
        this.name = name;
    }

    public color() {
        console.log(`${this.name} the animal's color is 'default'. (Invisible?)`);
    }

    public speak() {
        console.log(`${this.name} the animal makes a sound.`);
    }
}
