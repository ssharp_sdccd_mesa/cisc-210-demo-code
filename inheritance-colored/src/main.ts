"use strict";

import { Color } from './Color';
import { NoisyAnimal } from './NoisyAnimal';
import { Animal } from './Animal';
import { Cat } from './Cat';
import { Dog } from './Dog';
import { HouseCat } from './HouseCat';
import { MaineCoonCat } from './MaineCoonCat';


const animal = new Animal('George');
animal.speak();

const dog1 = new Dog('Fido', 'red');
dog1.speak();

const genericat1 = new Cat('Ash', 'pink & purple');
genericat1.speak();

const cat1 = new HouseCat('Notso', 'grey');
cat1.speak();

const cat2 = new MaineCoonCat('Hobo', 'black');
cat2.speak();

const noisyAnimals: NoisyAnimal[] = [ animal, dog1, cat1, cat2 ];

console.log("\nLet all the animals speak");
noisyAnimals.forEach((anml) => {
    anml.speak();
});

const partyAnimals: Color[] = [ animal, dog1, cat1, cat2 ];

console.log("\nLet all the animals party");
partyAnimals.forEach((anml) => {
    anml.color();
});
