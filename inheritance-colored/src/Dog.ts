"use strict";

import { Animal } from './Animal';

export class Dog extends Animal {
    private theColor: string;

    public constructor(name: string, color: string) {
        super(name); // call the super class constructor and pass in the name parameter
        this.theColor = color;
    }

    public speak() {
        console.log(`${this.name} the dog barks.`);
    }

    public color(): void {
        console.log(`${this.name} the dog's color is ${this.theColor}.`);
    }
}
