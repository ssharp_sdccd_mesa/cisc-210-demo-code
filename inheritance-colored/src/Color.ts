"use strict";

export interface Color {
    color(): void;
}
