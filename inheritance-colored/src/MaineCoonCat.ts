"use strict";

import { Cat } from './Cat';

export class MaineCoonCat extends Cat {

    public constructor(name: string, color: string) {
        super(name, color); // call the super class constructor and pass in the name parameter
    }

    public speak() {
        console.log(`${this.name} the Maine Coon cat whines.`);
    }
}
