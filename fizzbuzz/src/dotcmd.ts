"use strict";

import { ChainCommand } from './chaincommand';
import { Context } from './context';
import { FBCmd } from './fbcmd';

export class DotCmd extends FBCmd {

    public constructor(next: ChainCommand | undefined) {
        super(next);
    }

    public doNext(num: number): void {
        let ctx: Context = Context.getInstance();
        let fb = ctx.getString();
        if (fb.length === 0) {
            ctx.setString('.');
        }
        this.invokeNextCmd(num);
    }
}
