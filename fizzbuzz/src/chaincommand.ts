"use strict";

export interface ChainCommand {
    doNext(num: number): void;
}
