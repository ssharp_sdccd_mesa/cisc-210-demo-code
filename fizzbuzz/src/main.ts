"use strict";

import { stdout } from 'process';

import { ChainCommand } from './chaincommand';
import { Context } from './context';
import { Fizz } from './fizz';
import { Buzz } from './buzz';
import { DotCmd } from './dotcmd';

class Controller {
    private chain: ChainCommand

    public constructor() {
        this.chain = this.setup();
    }

    private setup(): ChainCommand {
        let dot: ChainCommand = new DotCmd(undefined);
        let buzz: ChainCommand = new Buzz(dot);
        let fizz: ChainCommand = new Fizz(buzz);
        return fizz;
    }

    public main(num: number) {
        let ctx = Context.getInstance();
        for (let i = 1; i <= num; i++) {
            ctx.setString('');
            this.chain.doNext(i);
            stdout.write(ctx.getString() + ' ');
        }
        stdout.write('\n');
    }
}

let cntrl : Controller = new Controller();
cntrl.main(17);
