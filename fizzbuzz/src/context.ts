"use strict";

export class Context {
    private static ctx: Context = new Context();

    private str: string;

    public static getInstance(): Context {
        return Context.ctx;
    }

    private constructor() {
        this.str = '';
    }

    public getString() : string {
        return this.str;
    }

    public setString(str: string) : void {
        this.str = str;
    }
}
