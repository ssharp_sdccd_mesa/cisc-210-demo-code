"use strict";

import { ChainCommand } from './chaincommand';
import { Context } from './context';
import { FBCmd } from './fbcmd';

export class Buzz extends FBCmd {
    public constructor(next: ChainCommand | undefined) {
        super(next);
    }

    public doNext(num: number): void {
        let ctx: Context = Context.getInstance();
        let fb = ctx.getString();
        if ((num % 5) === 0) {
            fb += 'Buzz';
            ctx.setString(fb);
        }
        this.invokeNextCmd(num);
    }
}
