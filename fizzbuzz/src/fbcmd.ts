"use strict";

import { ChainCommand } from './chaincommand';

export abstract class FBCmd implements ChainCommand {
    private next: ChainCommand | undefined;

    public constructor(next: ChainCommand | undefined) {
        this.next = next;
    }

    protected invokeNextCmd(num: number): void {
        if (this.next) {
            this.next.doNext(num);
        }
    }

    public abstract doNext(num: number): void;
}
