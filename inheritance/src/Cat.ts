"use strict";

import { Animal } from './Animal';

export class Cat extends Animal {
    public constructor(name: string) {
        super(name); // call the super class constructor and pass in the name parameter
    }
}
