"use strict";

import { NoisyAnimal } from './NoisyAnimal';

export class Animal implements NoisyAnimal {
    protected name: string;

    public constructor(name: string) {
        this.name = name;
    }

    public speak() {
        console.log(`${this.name} the animal makes a sound.`);
    }
}
