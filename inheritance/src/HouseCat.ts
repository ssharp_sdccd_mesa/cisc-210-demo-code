"use strict";

import { Cat } from './Cat';

export class HouseCat extends Cat {
    public constructor(name: string) {
        super(name); // call the super class constructor and pass in the name parameter
    }

    public speak() {
        console.log(`${this.name} the house cat doesn't say anything and just stares at you.`);
    }
}
