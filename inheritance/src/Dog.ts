"use strict";

import { Animal } from './Animal';

export class Dog extends Animal {

    public constructor(name: string) {
        super(name); // call the super class constructor and pass in the name parameter
    }

    public speak() {
        console.log(`${this.name} the dog barks.`);
    }
}
