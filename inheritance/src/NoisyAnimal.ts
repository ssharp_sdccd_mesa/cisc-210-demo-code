"use strict";

export interface NoisyAnimal {
  speak(): void;
}
