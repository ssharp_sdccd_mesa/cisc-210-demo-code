"use strict";

import { Context } from './context';
import { Event } from './event';
import { Subscriber } from './subscriber';

export class Dot implements Subscriber {

    public handleEvent(event: Event): void {
        let ctx: Context = Context.getInstance();
        let fb = ctx.getString();
        if (fb.length === 0) {
            ctx.setString('.');
        }
    }
}
