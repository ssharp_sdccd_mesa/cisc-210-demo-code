"use strict";

import { Context } from './context';
import { Event } from './event';
import { Subscriber } from './subscriber';

export class Fizz implements Subscriber {

    public handleEvent(event: Event): void {
        let ctx: Context = Context.getInstance();
        let fb = ctx.getString();
        if ((event.num % 3) === 0) {
            fb += 'Fizz';
            ctx.setString(fb);
        }
    }
}
