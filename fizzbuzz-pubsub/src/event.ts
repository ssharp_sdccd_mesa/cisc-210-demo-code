"use strict";

export class Event {
    private newNum: number;

    constructor(newNum: number) {
        this.newNum = newNum;
    }

    public get num() {
        return this.newNum;
    }
}
