"use strict";

import { Context } from './context';
import { Event } from './event';
import { Subscriber } from './subscriber';

export class Buzz implements Subscriber {

    public handleEvent(event: Event): void {
        let ctx: Context = Context.getInstance();
        let fb = ctx.getString();
        if ((event.num % 5) === 0) {
            fb += 'Buzz';
            ctx.setString(fb);
        }
    }
}
