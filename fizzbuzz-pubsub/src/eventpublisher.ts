"use strict";

import { Event } from './event';
import { Publisher } from './publisher';
import { Subscriber } from './subscriber';

export class EventPublisher implements Publisher {

    private subs: Array<Subscriber> = [];

    public addSubscriber(sub: Subscriber): void {
        this.subs.push(sub);
    }

    public publishEvent(event: Event): void {
        for (let sub of this.subs) {
            sub.handleEvent(event);
        }
    }
}
