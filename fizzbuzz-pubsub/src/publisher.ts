"use strict";

import { Event } from './event';
import { Subscriber } from './subscriber';

export interface Publisher {
    addSubscriber(sub: Subscriber): void;
    publishEvent(event: Event): void;
}
