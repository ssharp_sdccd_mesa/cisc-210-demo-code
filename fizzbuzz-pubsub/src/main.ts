"use strict";

import { stdout } from 'process';

import { Context } from './context';
import { Fizz } from './fizz';
import { Buzz } from './buzz';
import { Dot } from './dot';
import { Counter } from './counter';
import { Subscriber } from './subscriber';

class Controller {
    private counter: Counter

    public constructor() {
        this.counter = this.setup();
    }

    private setup(): Counter {
        let pub = new Counter();
        let fizz: Subscriber = new Fizz();
        pub.addSubscriber(fizz);
        let buzz: Subscriber = new Buzz();
        pub.addSubscriber(buzz);
        let dot: Subscriber = new Dot();
        pub.addSubscriber(dot);
        return pub;
    }

    public main(num: number) {
        let ctx = Context.getInstance();
        for (let i = 1; i <= num; i++) {
            ctx.setString('');
            this.counter.recount();
            stdout.write(ctx.getString() + ' ');
        }
        stdout.write('\n');
    }
}

let cntrl : Controller = new Controller();
cntrl.main(17);
