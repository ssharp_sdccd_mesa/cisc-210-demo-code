"use strict";

import { Event } from './event';

export interface Subscriber {
    handleEvent(event: Event): void;
}
