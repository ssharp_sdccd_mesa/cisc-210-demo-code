"use strict";

import { Event } from './event';
import { EventPublisher } from './eventpublisher';
import { Subscriber } from './subscriber';

// This is the Subject, being observed.
export class Counter extends EventPublisher {

    private count: number = 0;

    public recount() {
        this.count += 1;
        const event = new Event(this.count);
        this.publishEvent(event);
    }
}
