"use strict";

// Global variable scope
let globalVar: string = "Global variable";

class Clazz {
    // Class scope public variable
    public static clazzPublicVar: string = "Class public variable";

    // Class scope private variable
    private static clazzPrivateVar = "Class private variable";

    // Instance scope public variable
    public instancePublicVar: string = "Instance public variable";

    // Instance scope private variable
    private instancePrivateVar: string = "Instance private variable";

    public static setClassPrivate(s: string) {
        Clazz.clazzPrivateVar = s;
    }

    public setInstancePrivate(s: string) {
        this.instancePrivateVar = s;
    }

    public printVars() {
        console.log("Global is " + globalVar);
        console.log("Class public is " + Clazz.clazzPublicVar);
        console.log("Class private is " + Clazz.clazzPrivateVar);
        console.log("Instance public is " + this.instancePublicVar);
        console.log("Instance private is " + this.instancePrivateVar);
    }
}

console.log("\nBefore anything");
console.log("Global is " + globalVar);
console.log("Class public is " + Clazz.clazzPublicVar);
//console.log("Class private is " + Clazz.clazzPrivateVar);

let c1: Clazz = new Clazz();
console.log("\nNew instance: c1");
c1.printVars();
globalVar += " c1";
Clazz.clazzPublicVar += " c1";
// Clazz.clazzPrivateVar += " c1";
Clazz.setClassPrivate("Class private" + " c1");
c1.instancePublicVar += " c1";
// c1.instancePrivateVar += " c1";
c1.setInstancePrivate("Class private" + " c1");
console.log("\nOld instance: c1");
c1.printVars();

let c2: Clazz = new Clazz();
console.log("\nNew instance: c2");
c2.printVars();
globalVar += " c2";
Clazz.clazzPublicVar += " c2";
// Clazz.clazzPrivateVar += " c2";
Clazz.setClassPrivate("Class private" + " c2");
c2.instancePublicVar += " c2";
// c2.instancePrivateVar += " c2";
c2.setInstancePrivate("Class private" + " c2");
console.log("\nOld instance: c2");
c2.printVars();
